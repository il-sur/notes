package ru.itpark.notes.service;

import org.springframework.stereotype.Service;
import ru.itpark.notes.domain.Note;
import ru.itpark.notes.repository.NoteRepository;

import java.util.List;

@Service
public class NoteService {
    private NoteRepository noteRepository;

    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public List<Note> findAll() {
        return noteRepository.findAll();
    }

    public Note findById(int id) {
        return noteRepository.findById(id);
    }

    public void removeById(int id) {
        noteRepository.removeById(id);
    }

    public void add(Note note) {
        noteRepository.add(note);
    }
}
