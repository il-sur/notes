package ru.itpark.notes.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.notes.domain.Account;
import ru.itpark.notes.domain.Note;
import ru.itpark.notes.service.NoteService;

import java.util.List;

@Controller
@RequestMapping("/")
public class NotesController {
    private NoteService noteService;

    public NotesController(NoteService noteService) {
        this.noteService = noteService;
    }

    /**
     *
     * @param model модель для передачи данных между controller & view
     * @param account информация о пользователе
     * @return имя view
     */
    @GetMapping
    @PreAuthorize("hasAuthority('VIEW')")
    public String getAll(Model model, @AuthenticationPrincipal Account account) {
        System.out.println(account);
        model.addAttribute("notes", noteService.findAll());
        model.addAttribute("account", account);

        return "notes";
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('VIEW')")
    public String get(@PathVariable int id, Model model, @AuthenticationPrincipal Account account) {
        model.addAttribute("note", noteService.findById(id));
        model.addAttribute("account", account);

        return "note";
    }

    @GetMapping("/add")
    @PreAuthorize("hasAuthority('EDIT')")
    public String addForm() {
        return "note-add";
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('EDIT')")
    public String add(@ModelAttribute Note note) {
        noteService.add(note);

        return "redirect:/";
    }

    @PostMapping("/remove/{id}")
    @PreAuthorize("hasAuthority('REMOVE')")
    public String remove(@PathVariable int id) {
        noteService.removeById(id);

        return "redirect:/";
    }
}
