package ru.itpark.notes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.notes.domain.Account;
import ru.itpark.notes.domain.Note;
import ru.itpark.notes.repository.AccountRepository;
import ru.itpark.notes.repository.NoteRepository;
import java.util.List;

@SpringBootApplication
public class NotesApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(NotesApplication.class, args);
        PasswordEncoder passwordEncoder = context.getBean(PasswordEncoder.class);
        AccountRepository accountRepository = context.getBean(AccountRepository.class);
        NoteRepository noteRepository = context.getBean(NoteRepository.class);

        accountRepository.add(new Account(
                0,
                "admin",
                passwordEncoder.encode("password"),
                List.of(
                        new SimpleGrantedAuthority("VIEW"),
                        new SimpleGrantedAuthority("EDIT"),
                        new SimpleGrantedAuthority("REMOVE")
                ),
                true,
                true,
                true,
                true
        ));
        accountRepository.add(new Account(
                0,
                "user",
                passwordEncoder.encode("password"),
                List.of(
                        new SimpleGrantedAuthority("VIEW")
                ),
                true,
                true,
                true,
                true
        ));

        noteRepository.add(new Note(0, "First", "Some content"));
        noteRepository.add(new Note(0, "Second", "Some content"));
        noteRepository.add(new Note(0, "Third", "Some content"));
    }
}
