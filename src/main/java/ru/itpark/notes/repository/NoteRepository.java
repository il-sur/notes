package ru.itpark.notes.repository;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.itpark.notes.domain.Note;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class NoteRepository {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public NoteRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Note> findAll() {
        return jdbcTemplate.query(
                "SELECT id, name, content FROM notes",
                (resultSet, i) -> new Note(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("content")
                )
        );
    }

    public Note findById(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, name, content FROM notes WHERE id = :id",
                Map.of("id", id),
                (resultSet, i) -> new Note(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("content")
                )
        );
    }

    public void removeById(int id) {
        jdbcTemplate.update("DELETE FROM notes WHERE id = :id", Map.of("id", id));
    }

    public void add(Note note) {
        jdbcTemplate.update(
                "INSERT INTO notes (name, content) VALUES (:name, :content)",
                Map.of(
                        "name", note.getName(),
                        "content", note.getContent()
                )
        );
    }
}
