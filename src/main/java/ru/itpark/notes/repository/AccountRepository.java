package ru.itpark.notes.repository;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import ru.itpark.notes.domain.Account;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class AccountRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public AccountRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int add(Account account) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(
                "INSERT INTO accounts (username, password) VALUES(:username, :password)",
                new BeanPropertySqlParameterSource(account),
                keyHolder
        );

        int id = keyHolder.getKey().intValue();
        account.getAuthorities().forEach(authority -> {
            jdbcTemplate.update(
                    "INSERT INTO authorities (accountId, authority) VALUES (:accountId, :authority)",
                    Map.of(
                            "accountId", id,
                            "authority", authority.getAuthority()
                    )
            );
        });

        return id;
    }

    public Account findByUsername(String name) {
        Account account = jdbcTemplate.queryForObject(
                "SELECT id, username, password, enabled, accountNonExpired, accountNonLocked, credentialsNonExpired FROM accounts WHERE username = :name",
                Map.of("name", name),
                (rs, i) -> new Account(
                        rs.getInt("id"),
                        rs.getString("username"),
                        rs.getString("password"),
                        Collections.emptyList(),
                        rs.getBoolean("enabled"),
                        rs.getBoolean("accountNonExpired"),
                        rs.getBoolean("accountNonLocked"),
                        rs.getBoolean("credentialsNonExpired")
                )

        );

        account.setAuthorities(
                jdbcTemplate.query(
                        "SELECT authority FROM authorities WHERE accountId = :accountId",
                        Map.of("accountId", account.getId()),
                        (rs, rowNum) -> rs.getString("authority")
                ).stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList())
        );

        return account;
    }
}
